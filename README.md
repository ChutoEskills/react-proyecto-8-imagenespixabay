# React Proyecto 8 - ImagenesPixabay
Obtención de imágenes de la plataforma Pixabay

[Link to Web:](https://loving-cray-0805f6.netlify.app/)

## ¿Herramientas utilizadas?
- Bootswatch
- useState
- Pixabay API
- Ajax Request/Promises


## ¿Cómo funciona?
- La API de Pixabay nos permite realizar una consulta utilizando una palabra clave para obtener un conjunto de imágenes, las cuales se organizan... cada imagene posee un contador de "Likes" y "Views"


## ¿Dudas?
